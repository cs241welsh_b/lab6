﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6
{
    public static class Merge<Type> where Type : IComparable<Type>
    {
        public static void mergeSort(Type[] thingToSort)
        {
            // First time mergeSort is called (indicated by using only an array parameter)
            // Allocate memory for our aux array and call mergeSort(array, lo, hi) on thingToSort
            aux = new Type[thingToSort.Length];
            mergeSort(thingToSort, 0, thingToSort.Length);

        }

        public static void mergeSort(Type[] thingToSort, int lo, int hi)
        {
            // if hi <= lo (base case) we can just return
            if ( hi > lo)
            {
                // Calculate the midpoint of thingToSort
                int mid = lo + (hi - lo) / 2;

                // Now sort both halves
                mergeSort(thingToSort, lo, mid);
                mergeSort(thingToSort, mid + 1, hi);

                // Finally, merge the results

            }
        }

        public static void merge(Type[] thingToSort, int lo, int mid, int hi)
        {
            // Copy the array to aux
            for(int k = 0; k<thingToSort.Length; k++)
            {
                aux[k] = thingToSort[k];
            }

            // i is the index for the first half
            int i = lo;
            // and j is for the second half
            int j = mid + 1;

            // k is used to traverse thingToSort
            for (int k = lo; k < hi; k++)
            {
                // If there is nothing left in the first half,
                // start copying from the second half
                if (i > mid) thingToSort[k] = aux[j++];

                // If there is nothing left in the second half,
                // start copying from the first half
                else if (i > hi) thingToSort[k] = aux[i++];

                // If the next value in the second half is less than the 
                // next value in the first half, take the value from the
                // second half and increment j
                else if (aux[j].CompareTo(aux[i]) < 0) thingToSort[k] = aux[j++];

                // Else, the first half has the next value we need
                // copy it and increment i
                else thingToSort[k] = aux[i++];
            }

        }

        // Auxillary array to be used for merges
        static Type[] aux;
    }
}
