﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Lab6
{
    class SortTests
    {

        public static void exchange(int[] array,
            int firstIndex, int secondIndex)
        {
            int temp = array[firstIndex];
            array[firstIndex] = array[secondIndex];
            array[secondIndex] = temp;
        }

        public static void fill(int[] arrayToFill, Random RNG)
        {
            for (int i = 0; i < arrayToFill.Length; i++)
            {
                arrayToFill[i] = RNG.Next(100000);
            }
        }

        public static void selectionSort(int[] thingToSort)
        {
            // For each element i in the array, look at every element past it in the array
            // (i+1, i+2 ... and keep track of the smallest item
            // If the smallest item is smaller than i, exchange them
            // Do this for every element in the array
            for (int i = 0; i < thingToSort.Length; i++)
            {
                for (int j = i + 1; j < thingToSort.Length; j++)
                {
                    if (thingToSort[j] < thingToSort[i])
                    {
                        exchange(thingToSort, i, j);
                    }
                }
            }
        }

        public static void shellSort(int[] thingToSort)
        {
            // Use Knuth's algorithm to determine the appropriate stride for thingToSort
            int stride = 1;
            while (stride < thingToSort.Length / 3) stride = (3 * stride) + 1;

            // We start at the end of the array  and work backwards with interlaced arrays
            // of length stride
            while(stride >= 1)
            {
                // i starts at stride and increments by 1
                for(int i = stride; i < thingToSort.Length; i++)
                {
                    // j starts at i then decrements by the stride as long as j is >= stride
                    // (so that we don't go past array[0], AND as long as this value needs to 
                    // be swapped with the j - stride element
                    for (int j = i; j >= stride && (thingToSort[j] < thingToSort[j - stride]); j -= stride)
                    {
                        exchange(thingToSort, j, j - stride);
                    }
                  
                }
                // Reduce stride by a third
                stride = stride / 3;
            }
        }

        public static void insertionSort (int[] thingToSort)
        {
            // First, find the smallest item in the array and place it at array[0] as a sentinel
            // array[0] is smallest at first
            int smallest = thingToSort[0];
            int smallestIndex = 0;

            for (int i = 1; i < thingToSort.Length; i++)
                if (smallest > thingToSort[i])
                {
                    // If we find a smaller value, set smallest and smallestIndex accordingly
                    smallest = thingToSort[i];
                    smallestIndex = i;
                }
            // Now exchange the smallest value and array[0]
            exchange(thingToSort, 0, smallestIndex);

            // For each array[i], compare it with all other values before i
            for (int i = 1; i < thingToSort.Length; i++)
            {
                // array[i] is the value we're comparing
                int insertingValue = thingToSort[i];

                int j;
                // for every value before array[i] that is greater than array[i]
                for (j = i - 1; thingToSort[j] > insertingValue;
                    j--)
                {
                    // move each value to the right one index
                    thingToSort[j + 1] = thingToSort[j];
                }
                // After each value that is > array[i] has been moved, move the old
                // array[i] to the left of these larger values
                thingToSort[j + 1] = insertingValue;
            }
        }

        public static void noSentinelInsertion(int[] thingToSort)
        {
            // A implementation of insertion sort that doesn't use a sentinel at array[0]

            // For each array[i], compare it with all other values before i
            for (int i = 1; i < thingToSort.Length; i++)
            {
                int j;
                // for every value before array[i] that is greater than array[i]
                for (j = i; j > 0 && thingToSort[j] < thingToSort[j-1];
                    j--)
                {
                    // move each value to the right one index
                    exchange(thingToSort, j, j - 1);
                }
                // After each value that is > array[i] has been moved, move the old
                // array[i] to the left of these larger values
                //thingToSort[j + 1] = insertingValue;
            }
        }


        public static void Main(string[] args)
        {
            // Initialize the timer for the tests
            Stopwatch timer = new Stopwatch();

            // Initialize RNG (with a constant seed for testing)
            Random RNG = new Random(5000);

            // Declare an int[1024] array and fill it with random numbers
            int[] selectionSortArray = new int[1024];
            int[] shellSortArray = new int[1024];
            int[] insertionSortArray = new int[1024];
            int[] noSentinelArray = new int[1024];
            int[] mergeSortArray = new int[1024];

            // Variable to resize array after each loop of tests
            int newArraySize = 1024;

            // Variables to track how much faster insertion sort with sentinel
            // is compared to the no-sentinel version
            long insertionTime = 0;
            long noSentinelTime = 0;
            long average = 0;
            int numLoops = 0;
            long[] difference = new long[8];

            Console.WriteLine("Tests will take a little over a minute and a half...\n");
            // Array sizes longer than 131072 take too long to finish
            while (newArraySize < 131072)
            {
               

                // Resize each array to the new doubled array size
                Array.Resize(ref selectionSortArray, newArraySize);
                Array.Resize(ref shellSortArray, newArraySize);
                Array.Resize(ref insertionSortArray, newArraySize);
                Array.Resize(ref mergeSortArray, newArraySize);
                Array.Resize(ref noSentinelArray, newArraySize);

                // Fill selectionSortArray with more random numbers, then copy these over
                // to every other array, so that the contents of each array are the same and 
                // do not affect testing speeds
                fill(selectionSortArray, RNG);
                selectionSortArray.CopyTo(shellSortArray, 0);
                selectionSortArray.CopyTo(insertionSortArray, 0);
                selectionSortArray.CopyTo(mergeSortArray, 0);
                selectionSortArray.CopyTo(noSentinelArray, 0);

                //Start the timer, run a selection sort on the array, and stop the timer
                timer.Start();
                selectionSort(selectionSortArray);
                timer.Stop();

                Console.WriteLine("Selection sort time (in ticks) for a {0} int array: {1}",
                                    selectionSortArray.Length, timer.ElapsedTicks);

                //Reset the timer
                timer.Reset();

                //Start the timer, run a shell sort on the array, and stop the timer
                timer.Start();
                shellSort(shellSortArray);
                timer.Stop();

                Console.WriteLine("Shell sort time (in ticks) for a {0} int array: {1}",
                                    shellSortArray.Length, timer.ElapsedTicks);

                //Reset the timer
                timer.Reset();

                //Start the timer, run a shell sort on the array, and stop the timer
                timer.Start();
                insertionSort(insertionSortArray);
                timer.Stop();
                insertionTime = timer.ElapsedTicks;

                Console.WriteLine("Insertion sort time (in ticks) for a {0} int array: {1}",
                                    insertionSortArray.Length, timer.ElapsedTicks);

                //Reset the timer
                timer.Reset();

                //Start the timer, run a shell sort on the array, and stop the timer
                timer.Start();
                noSentinelInsertion(noSentinelArray);
                timer.Stop();
                noSentinelTime = timer.ElapsedTicks;

                // Calculate the difference in speed between sentinel
                // and no-sentinel insertion sorts, and add them to an array
                difference[numLoops] = noSentinelTime - insertionTime;

                Console.WriteLine("No sentinel insertion sort time (in ticks) for a {0} int array: {1}",
                                    noSentinelArray.Length, timer.ElapsedTicks);

                //Reset the timer
                timer.Reset();

                //Start the timer, run a shell sort on the array, and stop the timer
                timer.Start();
                Merge<int>.mergeSort(mergeSortArray);
                timer.Stop();

                Console.WriteLine("Merge sort time (in ticks) for a {0} int array: {1}",
                                    mergeSortArray.Length, timer.ElapsedTicks);
                Console.WriteLine();

                //Reset the timer
                timer.Reset();

                // Double the array size
                newArraySize *= 2;

                // Increment loop counter
                numLoops++;
            }

            // Calculate the average difference  in speed between 
            // the different insertion sorts
            foreach(long x in difference)
            {
                average += x;
            }
            average /= numLoops;

            Console.WriteLine("On average, insertion sort WITH a sentinel is {0} ticks faster than the" +
                                " no-sentinel insertion sort.", average);
            Console.WriteLine();
            Console.WriteLine("Insertion sort is better only on very small size N.");
            Console.WriteLine();
            Console.WriteLine("Tests finished, hit any key to exit.");
            Console.ReadLine();
        }
    }
}
